import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class RupeeTest {
    @Test
    void checksamerupee() {

        assertEquals(new Rupee(10),new Rupee(10));
    }
    @Test
    void checknullrupee() {
        assertNotEquals(new Rupee(10),null);
    }

    @Test
    void checkdifferentType()
    {
        assertNotEquals(new Rupee(10),new Object());
    }

    @Test
    void checksum()
    {
        assertEquals(new Rupee(10),new Rupee(7).add(new Rupee(3)));
    }

}
