import java.util.Objects;

public class Rupee {
    int denomination;

    public Rupee(int denomination)
 {
     this.denomination = denomination;
 }
    /*@Override
    public boolean equals(Object obj) {
        if(obj == null || getClass()!=obj.getClass())
            return false;
        return denomination == ((Rupee) obj).denomination;

    }
 */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rupee rupee = (Rupee) o;
        return denomination == rupee.denomination;
    }

    @Override
    public int hashCode() {
        return Objects.hash(denomination);
    }

    public Rupee add(Rupee rupee)
    {
        return new  Rupee (denomination + rupee.denomination);
    }
}
